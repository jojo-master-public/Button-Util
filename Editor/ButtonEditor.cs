using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Dioinecail.MonoBehaviourEditor.Editors;

namespace Dioinecail.ButtonUtil.Editors
{
    public class ButtonInfo
    {
        public MethodInfo Method;
        public ButtonAttribute Attribute;
    }

    public class ButtonEditor : ICustomEditor
    {
        public EditorType Type => EditorType.After;

        private ButtonInfo[] _buttonMethods;



        public void OnEnable(SerializedObject target)
        {
            _buttonMethods = target.targetObject
                .GetType()
                .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(m => m.GetCustomAttribute(typeof(ButtonAttribute)) != null)
                .Select(m => new ButtonInfo() { Method = m, Attribute = (ButtonAttribute)m.GetCustomAttribute(typeof(ButtonAttribute)) })
                .ToArray();
        }

        public void OnInspectorGUI(SerializedObject target)
        {
            if (_buttonMethods != null && _buttonMethods.Length > 0)
            {
                EditorGUILayout.BeginVertical();

                foreach (var bm in _buttonMethods)
                {
                    var n = string.IsNullOrEmpty(bm.Attribute.Name)
                        ? bm.Method.Name
                        : bm.Attribute.Name;

                    if (GUILayout.Button(new GUIContent(n)))
                    {
                        bm.Method.Invoke(target.targetObject, null);
                    }
                }

                EditorGUILayout.EndVertical();
            }
        }
    }
}