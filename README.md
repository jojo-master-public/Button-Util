# Button Attribute Utility

Just a simple button attribute that you can put onto any method and it will be displayed in the editor  

## Dependencies  

com.dioinecail.monobehavioureditor : https://gitlab.com/jojo-master-public/custom-monobehaviour-editor.git  

## Installation

Open Unity Package Manager  
Click on a plus sign in top left corner  
Choose "Add package from git url.."  
Copy and paste dependency repo url https://gitlab.com/jojo-master-public/custom-monobehaviour-editor.git  
Hit Add  
Copy and paste this repository url https://gitlab.com/jojo-master-public/Button-Util.git  
Hit Add  
Enjoy!  

## Contact

Developed by Andrew Dioinecail.  
Twitter: [Dioinecail](https://twitter.com/dioinecail).  
Email: andrewdionecail@gmail.com  
  
Feel free to ask questions!  