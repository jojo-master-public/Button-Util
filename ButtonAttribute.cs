using System;

namespace Dioinecail.ButtonUtil
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ButtonAttribute : Attribute
    {
        public readonly string Name;

        public ButtonAttribute(string name = "")
        {
            this.Name = name;
        }
    }
}